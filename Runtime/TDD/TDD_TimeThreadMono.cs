using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TDD_TimeThreadMono : MonoBehaviour
{

    public TimeThreadMono m_timeThread;
    public float m_helloTime;
    public bool m_receivedTest;
    public int m_pingCount;
    private PingWhenItisTime m_switchButton;
    void Start()
    {
        PingWhenItisTime hello = new PingWhenItisTime(m_helloTime, Hello , PingThreadType.InUnityThread );
        PingWhenItisTime switchButton = new PingWhenItisTime(6f, SwitchReceivedBool, PingThreadType.InTimeThread);
        m_timeThread.Add(hello);
        m_timeThread.Add(switchButton);

       // InvokeRepeating("CheckSomething", 0, 2);
    }

    private void Update()
    {
        //CheckSomething();
    }


    public void AddPing() {
        m_pingCount++;
    }

    private void Hello()
    {
        Debug.Log("Hello");
    }

    private void SwitchReceivedBool()
    {
        m_receivedTest = true;
    }

}
